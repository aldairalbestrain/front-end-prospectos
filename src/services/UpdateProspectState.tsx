import axios, { AxiosRequestConfig } from 'axios';

const host: string = `https://prospects-server.herokuapp.com/api`;

type response = {
    message: string
}

type captureServiceType = {
    message?: string,
    error: {
        status: number,
        message: string[]
    } | null
}

export const updateProspectStateService = async (body: any, id: string): Promise<captureServiceType> => {

    try {
        const token = localStorage.getItem('token') || '';

        const options: AxiosRequestConfig = {
            method: 'PATCH',
            headers: { 
                'Content-Type': 'application/json',
                'x-token': token 
            },
            responseType: 'json'
        };

        const response = await axios.patch<response>(host + '/prospectos?id=' + id, body, options)

        return {
            message: response.data.message,
            error: null
        }
    } catch (error: any) {
        console.error(error?.message)

        return {
            error: {
                status: error.response?.status,
                message: error.response.data.message
            }
        }
    }
}
