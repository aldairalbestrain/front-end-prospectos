import axios, { AxiosRequestConfig } from 'axios';

const host: string = `https://prospects-server.herokuapp.com/api`;

export interface Prospect {
    _id: string,
    apellidoMaterno: string
    apellidoPaterno: string
    calle: string
    codigoPostal: string
    colonia: string
    documents: string[]
    nombre: string
    numeroDeCalle: string
    rfc: string
    status: string
    telefono: string
    observaciones?: string
}


type response = {
    prospectos: Prospect[],
    total: number
}

type listServiceType = {
    prospectos: Prospect[] | null,
    total: number,
    message?: string,
    error: {
        status: number,
        message: string[]
    } | null
}

type oneProspectServiceType = {
    prospect: Prospect | null,
    message?: string,
    error: {
        status: number,
        message: string[]
    } | null
}

export const getProspects = async (page: number): Promise<listServiceType> => {

    try {
        const token = localStorage.getItem('token') || '';
        const options: AxiosRequestConfig = {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'x-token': token 
            },
            responseType: 'json'
        };

        const response = await axios.get<response>(host + '/prospectos/list?page=' + page, options)
        return {
            prospectos: response.data.prospectos,
            total: response.data.total,
            message: '',
            error: null
        }
    } catch (error: any) {
        console.error(error?.message)

        return {
            prospectos: null,
            total: 0,
            error: {
                status: error.response?.status,
                message: error.response?.data?.message
            }
        }
    }
}

type responseOneProspect = {
    prospect: Prospect
}

export const getProspectById = async (id: string): Promise<oneProspectServiceType> => {

    try {
        const token = localStorage.getItem('token') || '';
        const options: AxiosRequestConfig = {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'x-token': token 
            },
            responseType: 'json'
        };

        const response = await axios.get<responseOneProspect>(host + '/prospectos?id=' + id, options)

        return {
            prospect: response.data.prospect,
            message: '',
            error: null
        }
    } catch (error: any) {
        console.error(error?.message)

        return {
            prospect: null,
            error: {
                status: error.response?.status,
                message: error.response?.data?.message
            }
        }
    }
}
