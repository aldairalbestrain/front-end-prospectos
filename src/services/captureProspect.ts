import axios, { AxiosRequestConfig } from 'axios';

const host: string = `https://prospects-server.herokuapp.com/api`;

type response = {
    message: string,
    token: string
}

type captureServiceType = {
    message?: string,
    error: {
        status: number,
        message: string[]
    } | null
}

export const captureProspectService = async (body: any): Promise<captureServiceType> => {

    try {
        const token = localStorage.getItem('token') || '';
        const options: AxiosRequestConfig = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'x-token': token 
            },
            responseType: 'json'
        };

        const response = await axios.post<response>(host + '/prospectos', body, options)

        return {
            message: response.data.message,
            error: null
        }
    } catch (error: any) {
        console.error(error?.message)

        return {
            error: {
                status: error.response?.status,
                message: error.response.data.message
            }
        }
    }
}
