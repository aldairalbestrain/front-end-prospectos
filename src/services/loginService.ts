import axios from 'axios';

const host: string = `https://prospects-server.herokuapp.com/api`;

type response = {
    message: string,
    token: string
}

type loginServiceType = {
    error: {
        status: number,
        message: string[]
    } | null
}

export const loginService = async (body: any): Promise<loginServiceType> => {

    try {
        const response = await axios.post<response>(host + '/auth/login', body)
        localStorage.setItem("token", response.data.token);

        return {
            error: null
        }
    } catch (error: any) {
        console.error(error?.message)

        return {
            error: {
                status: error.response?.status,
                message: error.response.data.message
            }
        }
    }
}
