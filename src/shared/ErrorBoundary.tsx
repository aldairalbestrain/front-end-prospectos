import React from 'react';
import { ErrorMessage } from './ErrorMessage';
import 'react-toastify/dist/ReactToastify.css';
import { useHistory } from 'react-router';

type ServiceContainerInterface = {
    error: {
        status: number
        message: string[]
    } | null
    setState: React.Dispatch<React.SetStateAction<{
        status: number;
        message: string[];
    } | null>>

}

export const ErrorBoundary: React.FC<ServiceContainerInterface> = ({ children, error, setState }) => {
    const history = useHistory()
    const validateStatusError = () => {
        const status = error?.status || 0

        if (status === 401) {
            localStorage.removeItem('token')
            history.push('/login')
        }
        if (status === 401 || status === 404 || status >= 500) return <ErrorMessage /> //status === 401  redirecciona a login

        setState(null)
        return children
    }

    return (
        <>
            {
                !error ?
                    children
                    :
                    validateStatusError()
            }
        </>
    );
};
