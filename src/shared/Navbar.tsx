import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import { PersonAdd, Contacts } from '@mui/icons-material';
import { useHistory } from 'react-router';

type NavbarType = {
    section?: Section
}

type Section = 'list' | 'capture'

export const Navbar: React.FC<NavbarType> = ({ section }) => {
    const history = useHistory()
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" >
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        RECLUTAMIENTO
                    </Typography>
                    <IconButton onClick={() => history.push('/list')}> <Contacts style={{ color: section === 'list' ? 'white' : '' }}/> </IconButton>
                    <IconButton onClick={() => history.push('/capture')}> <PersonAdd style={{ color: section === 'capture' ? 'white' : '' }}/> </IconButton>
                    <Button color="inherit" onClick={() => {
                        localStorage.removeItem('token')
                        history.push('/login')
                    }}>Cerrar sesión</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}