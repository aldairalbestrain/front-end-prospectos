import { css } from '@emotion/css';
import * as React from 'react';

const container = css`
    padding: 43px 0px 0px 59px;
    background-color: white;
    height: 100%;
`;

export const PageContainer: React.FC = (props) => {
    return (
        <div className={container}>{props.children}</div>
    );
};
