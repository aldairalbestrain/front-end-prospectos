import { css } from '@emotion/css';
import { Skeleton } from '@mui/material';
import * as React from 'react';

interface IFormSkeletonProps {
}

const container = css`
    width: 100%;
    height: 75%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 50px 0px;
`

const cardClass = css`
    width: 375px;
    height: 100%;
    padding: 20px;
    position: relative;
`

export const FormSkeleton: React.FC<IFormSkeletonProps> = (props) => {
    return (
        <div className={container}>
            <Skeleton className={cardClass} style={{ height: '100%' }} animation="wave" variant="rectangular" />
        </div>
    );
};
