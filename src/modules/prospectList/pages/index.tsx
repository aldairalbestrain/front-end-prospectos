import * as React from 'react';
import { useHistory } from 'react-router';
import { ToastContainer } from 'react-toastify';
import { Navbar } from '../../../shared/Navbar';
import { ContainerList } from '../containerList/ContainerList';

interface IProspectListProps {
}

export const ProspectListPage: React.FC<IProspectListProps> = (props) => {

  const history = useHistory()

  const token = localStorage.getItem('token')
  if (!token) history.push('/login')

  return (
    <div style={{ height: '100vh', width: '100%', background: '#fbf8f8' }}>
      <Navbar section='list'/>
      <ContainerList />
      <ToastContainer />
    </div>
  );
};
