import { css } from '@emotion/css';
import { Skeleton } from '@mui/material';
import * as React from 'react';

interface IListSkeletonProps {
}

const divInformation = css`
    padding: 10px 70px 0px 70px;
    @media (max-width: 680px) {
        padding: 10px 20px 0px 20px !important;
    }
`

export const ListSkeleton: React.FC<IListSkeletonProps> = (props) => {
    return (
        <div style={{ width: '100%', height: '75vh' }}>
            <div className={divInformation}>
                <div>
                    <Skeleton animation="wave" variant="rectangular" width={200} height={48} style={{ margin: 0 }} />
                </div>
                {
                    Array.apply(0, Array(10)).map((element: any, i: number) => {
                        return <Skeleton key={i} animation="wave" variant="rectangular" width='100%' height={48} style={{ margin: '2px 0px' }} />
                    })
                }
            </div>
        </div>
    );
};
