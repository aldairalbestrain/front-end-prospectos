import { css } from '@emotion/css';
import { Accordion, AccordionDetails, AccordionSummary, Button, Card, CardContent, Chip, IconButton, LinearProgress, Pagination, Typography } from '@mui/material';
import * as React from 'react';
import { toast } from 'react-toastify';
import { getProspects, Prospect } from '../../../services/getProspects';
import { ErrorBoundary } from '../../../shared/ErrorBoundary';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import HomeIcon from '@mui/icons-material/Home';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import AddTaskIcon from '@mui/icons-material/AddTask';
import { ProspectDetails } from './ProspectDetails';
import { useHistory } from 'react-router';
import { ListSkeleton } from './ListSkeleton';

const container = css`
    width: 100%;
    height: 75%;
`

const paginationStyle = css`
    width: 100%;
    margin: 10px 0px;
    display: flex;
    justify-content: center;
`

const divInformation = css`
    padding: 10px 70px 0px 70px;
    @media (max-width: 680px) {
        padding: 10px 20px 0px 20px !important;
    }
`
const actionsContainer = css`
    display: flex;
    width: 100%;
    @media (max-width: 680px) {
        display: block;
        padding: 10px 20px 0px 20px !important;
    }
`

export type errorType = {
    status: number,
    message: string[]
} | null

export const ContainerList: React.FC = (props) => {
    const history = useHistory()
    const [error, setError] = React.useState<errorType>(null)
    const [prospectsList, setProspectList] = React.useState<Prospect[]>([])
    const [loading, setLoading] = React.useState(false)

    const [page, setPage] = React.useState(1)
    const [total, setTotal] = React.useState(0)

    React.useEffect(() => {
        getProspectsList(page)
    }, [page])

    const getProspectsList = async (numberPage: number) => {
        setLoading(true)

        const { prospectos, total: totalPages, error: serviceError } = await getProspects(numberPage)

        if (serviceError) {
            const status = serviceError?.status || 0
            if (status === 400 || (status >= 405 && status < 500)) serviceError?.message.map(err => {
                toast.error(err)
            })
            setError(serviceError)
        } else {
            setProspectList(prospectos || [])
            setTotal(totalPages)
        }
        setLoading(false)

    }

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => setPage(value)

    if (loading) return <ListSkeleton />

    return (
        <ErrorBoundary error={error} setState={setError}>
            <div className={container}>
                <div className={divInformation}>

                    <div style={{ display: 'flex', marginBottom: 10 }}>
                        <Typography variant="h5" component="h6">Prospectos</Typography>
                        <IconButton onClick={() => history.push('/capture')}> <AddCircleIcon /> </IconButton>
                    </div>

                    {
                        prospectsList.map((prospect) => {
                            const { nombre, colonia, calle, numeroDeCalle, status, _id } = prospect
                            const color = status === 'aceptado' ? 'green' : status === 'rechazado' ? 'red' : 'gray'

                            return <Accordion key={_id}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography>{nombre}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Typography style={{ display: 'flex', alignItems: 'center' }}>
                                        <HomeIcon style={{ marginRight: 10, color: '#1976d2' }} />{colonia}, {calle}, {numeroDeCalle}
                                    </Typography>

                                    <div className={actionsContainer}>
                                        <ProspectDetails prospecto={prospect} />
                                        <Button variant="outlined" onClick={() => history.push(`/evaluation/${_id}`)} startIcon={<AddTaskIcon />} style={{ marginTop: 15 }}>
                                            Evaluar
                                        </Button>

                                        <Chip
                                            label={'status: ' + status}
                                            variant="outlined"
                                            style={{ marginTop: 15, marginLeft: 10, color }}
                                        />
                                    </div>


                                </AccordionDetails>
                            </Accordion>
                        })
                    }
                    <div className={paginationStyle}>
                        <Pagination count={total} page={page} color="primary" onChange={handleChangePage} />
                    </div>
                </div>

            </div>
        </ErrorBoundary>
    );
}
