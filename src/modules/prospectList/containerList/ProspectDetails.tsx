import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Prospect } from '../../../services/getProspects';
import { Avatar, Chip, Divider, List, ListItem, ListItemAvatar, ListItemText } from '@mui/material';
import { AccountCircle, Home, Phone, Description } from '@mui/icons-material';
import { Link } from 'react-router-dom';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

type ProspectDetailType = {
    prospecto: Prospect
}

export const ProspectDetails: React.FC<ProspectDetailType> = ({
    prospecto: {
        nombre,
        apellidoPaterno,
        apellidoMaterno,
        calle,
        numeroDeCalle,
        colonia,
        codigoPostal,
        telefono,
        rfc,
        status,
        observaciones,
        documents
    }
}) => {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const color = status === 'autorizado' ? 'green' : status === 'rechazado' ? 'red' : 'gray'

    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen} startIcon={<VisibilityIcon />} style={{ marginTop: 15, marginRight: 10 }}>
                Ver más información
            </Button>
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle>INFORMACIÓN DEL PROSPECTO</DialogTitle>
                <DialogContent>
                    <List
                        sx={{
                            width: '100%',
                            maxWidth: 360,
                            bgcolor: 'background.paper',
                        }}
                    >
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <AccountCircle />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={nombre} secondary={apellidoPaterno + ' ' + apellidoMaterno} />
                        </ListItem>
                        <Divider variant="inset" component="li" />

                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <Home />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={colonia + ' ' + codigoPostal} secondary={calle + ' ' + numeroDeCalle} />
                        </ListItem>
                        <Divider variant="inset" component="li" />

                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <Phone />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={telefono} />
                        </ListItem>
                        <Divider variant="inset" component="li" />

                        <ListItem style={{display: 'block'}}>
                            {
                                documents.map((document, i) => {
                                    return <Chip
                                        icon={<Description style={{ color: 'blue' }} />}
                                        variant="outlined" style={{ marginRight: 5, margin: '5px 0px' }}
                                        label={document.split('/')[2] || 'Sin nombre'}
                                        key={i}
                                        deleteIcon={<VisibilityIcon />}
                                        onDelete={() => {
                                            window.open('https://prospects-server.herokuapp.com/api' + document, '_blank');
                                        }}
                                    />
                                })
                            }

                        </ListItem>
                        <Divider variant="inset" component="li" />



                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <Description />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={rfc} secondary={'Registro Federal de Contribuyentes'} />
                        </ListItem>
                        <Divider variant="inset" component="li" />

                        {observaciones !== '' && <> <ListItem>
                            <ListItemText primary={'Observaciones'} secondary={observaciones} />
                        </ListItem>
                            <Divider variant="inset" component="li" />
                        </>}
                    </List>
                    <Chip
                        label={'status: ' + status}
                        variant="outlined" style={{ marginRight: 5, color, position: 'absolute', bottom: '10px', left: '10px' }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cerrar</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}