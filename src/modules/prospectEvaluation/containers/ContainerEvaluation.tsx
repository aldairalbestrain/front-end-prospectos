import { css } from '@emotion/css';
import { Avatar, Card, CardContent, Divider, LinearProgress, List, ListItem, ListItemAvatar, ListItemText, Typography } from '@mui/material';
import * as React from 'react';
import { toast } from 'react-toastify';
import { getProspectById, Prospect } from '../../../services/getProspects';
import { ErrorBoundary } from '../../../shared/ErrorBoundary';
import { AccountCircle, Home, Phone, Description } from '@mui/icons-material';
import { useParams } from 'react-router';
import { EvaluationeForm } from './EvaluationForm';
import { FormSkeleton } from '../../../shared/FormSkeleton';

const container = css`
    width: 100%;
    height: 75%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 50px 0px;
`

const cardClass = css`
    width: 375px;
    height: 100%;
    padding: 20px;
    position: relative;
`

export type errorType = {
    status: number,
    message: string[]
} | null

export const ContainerEvaluation: React.FC = (props) => {
    const [error, setError] = React.useState<errorType>(null)
    const [loading, setLoading] = React.useState(false)
    const [prospectInfo, setProspectInfo] = React.useState<Prospect | null>(null)

    const { id } = useParams<{ id: string; }>();

    React.useEffect(() => {
        getProspect(id)
    }, [])

    const getProspect = async (prospectId: string) => {
        setLoading(true)
        const { prospect, error: serviceError } = await getProspectById(prospectId)

        if (serviceError) {
            const status = serviceError?.status || 0
            if (status === 400 || (status >= 405 && status < 500)) serviceError?.message.map(err => {
                toast.error(err)
            })
            setError(serviceError)
        } else {
            setProspectInfo(prospect)
        }
        setLoading(false)
    }

    if (loading) return <FormSkeleton />

    return (
        <ErrorBoundary error={error} setState={setError}>
            <div className={container}>
                <Card className={cardClass}>

                    {loading && <div style={{ position: 'absolute', top: 0, left: 0, width: '100%' }}><LinearProgress /></div>}

                    <Typography gutterBottom variant="h5" component="div" style={{ textAlign: 'center' }}>
                        Evaluar prospecto
                    </Typography>
                    <CardContent style={{ overflowY: 'scroll', height: '80%' }}>
                        <List
                            sx={{
                                width: '100%',
                                maxWidth: 360,
                                bgcolor: 'background.paper',
                            }}
                        >
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <AccountCircle />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={prospectInfo?.nombre} secondary={prospectInfo?.apellidoPaterno + ' ' + prospectInfo?.apellidoMaterno} />
                            </ListItem>
                            <Divider variant="inset" component="li" />

                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <Home />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={prospectInfo?.colonia + ' ' + prospectInfo?.codigoPostal} secondary={prospectInfo?.calle + ' ' + prospectInfo?.numeroDeCalle} />
                            </ListItem>
                            <Divider variant="inset" component="li" />

                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <Phone />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={prospectInfo?.telefono} />
                            </ListItem>
                            <Divider variant="inset" component="li" />

                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <Description />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={prospectInfo?.rfc} secondary={'Registro Federal de Contribuyentes'} />
                            </ListItem>
                            <Divider variant="inset" component="li" />
                        </List>

                        <EvaluationeForm loading={loading} setLoading={setLoading} setError={setError} prospectId={id}/>
                    </CardContent>
                </Card>
            </div>
        </ErrorBoundary>
    );
};
