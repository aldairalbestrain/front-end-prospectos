import * as React from 'react';
import { Theme, useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const status = [
  'aceptado',
  'rechazado'
];

type selectProps = {
    register: (status: string) => void,
    className: string
}

export const SelectStatus: React.FC<selectProps> = ({ register, className }) => {

  return (
    <>
      <FormControl sx={{ m: 1, width: 300 }} className={className}>
        <InputLabel id="demo-multiple-name-label">Status</InputLabel>
        <Select
          labelId="demo-multiple-name-label"
          id="demo-multiple-name"
          {...register("status")}
          input={<OutlinedInput label="Name" />}
          MenuProps={MenuProps}
        >
          {status.map((state) => (
            <MenuItem
              key={state}
              value={state}
            >
              {state}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
}