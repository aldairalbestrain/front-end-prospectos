import { css } from '@emotion/css';
import { Button, TextField } from '@mui/material';
import * as React from 'react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { errorType } from '../../prospectCapture/containers/CaptureContainer';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers/yup';
import { SelectStatus } from './SelectStatus';
import { updateProspectStateService } from '../../../services/UpdateProspectState';

const inputClass = css`
    width: 100% !important;
    margin: 10px 0px !important;
`

const buttonClass = css`
    width: 100%;
    margin: 20px 0px !important;
`

const errorMessage = css`
    font-size: 10px;
    color: red;
`

interface LoadingFormType {
    setError: React.Dispatch<React.SetStateAction<errorType | null>>;
    loading: boolean;
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
    prospectId: string
}

const requeredMessage = 'Campo requerido'

export const EvaluationeForm: React.FC<LoadingFormType> = ({ setError, setLoading, loading, prospectId }) => {
    const schema = yup.object({
        status: yup.string().required(requeredMessage),
        observaciones: yup.string().required(requeredMessage),
    }).required();

    const history = useHistory()
    const { register, handleSubmit, watch, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    });

    const onSubmit = async (data: any) => {
        setLoading(true)
        const dataResp = await updateProspectStateService(data, prospectId)

        if (dataResp.error) {
            const status = dataResp?.error?.status || 0
            if (status === 400 || (status >= 405 && status < 500)) dataResp.error?.message.map(err => {
                toast.error(err)
            })
            setError(dataResp.error)
        } else {
            toast.success(dataResp.message)
            history.push('/list')
        }

        setLoading(false)
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <SelectStatus register={register} className={inputClass}/>
            <p className={errorMessage}>{errors.status?.message}</p>

            <TextField
                id="outlined-textarea"
                rows={4}
                multiline
                label="Observaciones*"
                className={inputClass}
                {...register('observaciones')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.observaciones?.message}</p>

            <Button variant="contained" type="submit" className={buttonClass} disabled={loading}>Agregar</Button>
        </form>
    );
};
