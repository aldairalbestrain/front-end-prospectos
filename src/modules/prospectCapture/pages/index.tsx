import * as React from 'react';
import { useHistory } from 'react-router';
import { ToastContainer } from 'react-toastify';
import { Navbar } from '../../../shared/Navbar';
import { CaptureContainer } from '../containers/CaptureContainer';

export const ProspectCapturePage: React.FC = (props) => {

  const history = useHistory()

  const token = localStorage.getItem('token')
  if (!token) history.push('/login')

  return (
    <div style={{ height: '100vh', background: '#fbf8f8' }}>
      <Navbar section='capture'/>
      <CaptureContainer />
      <ToastContainer />
    </div>
  );
};
