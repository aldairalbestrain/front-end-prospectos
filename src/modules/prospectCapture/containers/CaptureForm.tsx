import { css } from '@emotion/css';
import { Button, TextField } from '@mui/material';
import * as React from 'react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { captureProspectService } from '../../../services/captureProspect';
import { errorType } from './CaptureContainer';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers/yup';
import { SelectFiles } from './SelectFiles';

const inputClass = css`
    width: 100%;
    margin: 10px 0px !important;
`

const buttonClass = css`
    width: 100%;
    margin: 20px 0px !important;
`

const errorMessage = css`
    font-size: 10px;
    color: red;
`

interface LoadingFormType {
    setError: React.Dispatch<React.SetStateAction<errorType | null>>;
    loading: boolean;
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}

export type fileType = {
    name: string | null,
    file: File | null
}

const requeredMessage = 'Campo requerido'

export const CaptureForm: React.FC<LoadingFormType> = ({ setError, setLoading, loading }) => {
    const schema = yup.object({
        nombre: yup.string().required(requeredMessage),
        apellidoPaterno: yup.string().required(requeredMessage),
        calle: yup.string().required(requeredMessage),
        numeroDeCalle: yup.number().positive().integer('Ingrese un numero entero').required(requeredMessage),
        colonia: yup.string().required(requeredMessage),
        codigoPostal: yup.number().positive().integer('Ingrese un numero entero').required(requeredMessage),
        telefono: yup.string().required(requeredMessage),
        rfc: yup.string().required(requeredMessage)
    }).required();

    const history = useHistory()
    const [ files, setFiles ] = React.useState<fileType[]>([])
    const { register, handleSubmit, watch, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    });

    const onSubmit = async (data: any) => {
        setLoading(true)

        var formData = new FormData();

        Object.keys(data).forEach(element => {
            formData.append(element, data[element]);
        });

        files.forEach(({file, name}) => {
            const fileName = name ? name.replace(/\s/g, '') : 'SinNombre'
            if (file) formData.append(fileName, file)
        })

        const dataResp = await captureProspectService(formData)

        if (dataResp.error) {
            const status = dataResp?.error?.status || 0
            if (status === 400 || (status >= 405 && status < 500)) dataResp.error?.message.map(err => {
                toast.error(err)
            })
            setError(dataResp.error)
        } else {
            toast.success(dataResp.message)
            history.push('/list')
        }

        setLoading(false)
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <TextField
                label="Nombre*"
                className={inputClass}
                {...register('nombre')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.nombre?.message}</p>

            <TextField
                label="Apellido paterno*"
                className={inputClass}
                {...register('apellidoPaterno')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.apellidoPaterno?.message}</p>
            
            <TextField
                label="Apellido materno"
                className={inputClass}
                {...register('apellidoMaterno')}
                disabled={loading}
            />

            <TextField
                label="Calle*"
                className={inputClass}
                {...register('calle')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.calle?.message}</p>
            
            <TextField
                label="Número de calle*"
                className={inputClass}
                {...register('numeroDeCalle')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.numeroDeCalle?.message}</p>
            
            <TextField
                label="Colonia*"
                className={inputClass}
                {...register('colonia')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.colonia?.message}</p>
            
            <TextField
                label="Código postal*"
                className={inputClass}
                {...register('codigoPostal')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.codigoPostal?.message}</p>
            
            <TextField
                label="Teléfono*"
                className={inputClass}
                {...register('telefono')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.telefono?.message}</p>
            
            <TextField
                label="RFC*"
                className={inputClass}
                {...register('rfc')}
                disabled={loading}
            />
            <p className={errorMessage}>{errors.rfc?.message}</p>
            
            <SelectFiles files={files} setFiles={setFiles}/>

            <Button variant="contained" type="submit" className={buttonClass} disabled={loading}>Agregar</Button>
        </form>
    );
};
