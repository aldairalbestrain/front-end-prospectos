import { css } from '@emotion/css';
import { Card, CardContent, LinearProgress, Typography } from '@mui/material';
import * as React from 'react';
import { ErrorBoundary } from '../../../shared/ErrorBoundary';
import { CaptureForm } from './CaptureForm';

const container = css`
    width: 100%;
    height: 75%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 50px 0px;
`

const cardClass = css`
    width: 375px;
    height: 100%;
    padding: 20px;
    position: relative;
`

export type errorType = {
    status: number,
    message: string[]
} | null

export const CaptureContainer: React.FC = (props) => {
  const [error, setError] = React.useState<errorType>(null)
  const [loading, setLoading] = React.useState(false)

  return (
      <ErrorBoundary error={error} setState={setError}>
          <div className={container}>
              <Card className={cardClass}>

                  {loading && <div style={{ position: 'absolute', top: 0, left: 0, width: '100%' }}><LinearProgress /></div>}

                  <Typography gutterBottom variant="h5" component="div" style={{ textAlign: 'center' }}>
                      Registrar prospecto
                  </Typography>
                  <CardContent style={{ overflowY: 'scroll', height: '80%' }}>
                    <CaptureForm loading={loading} setLoading={setLoading} setError={setError} />
                  </CardContent>
              </Card>
          </div>
      </ErrorBoundary>
  );
};
