import { css } from '@emotion/css';
import { Save, Description } from '@mui/icons-material';
import DeleteIcon from '@mui/icons-material/Delete';
import { Chip, IconButton, TextField } from '@mui/material';
import * as React from 'react';
import { toast } from 'react-toastify';
import { fileType } from './CaptureForm';

interface ISelectFilesProps {
    files: fileType[],
    setFiles: React.Dispatch<React.SetStateAction<fileType[]>>
}

const containerFile = css`
    padding: 10px 5px;
    border: 1px solid #e7e2e2;
    border-radius: 7px;
    margin: 10px 0px;
`

const input = css`
    margin: 10px 0px !important;
    width: 100%;
`
const errorMessage = css`
    font-size: 10px;
    color: #606000;
`

export const SelectFiles: React.FC<ISelectFilesProps> = ({ files, setFiles }) => {

    const [inputFile, setInputFile] = React.useState('')
    const [fileSelected, setFileSelected] = React.useState(null)
    const [error, setError] = React.useState('')

    return (
        <div>
            {
                <div className={containerFile}>
                    {files.length ?
                        files.map(({ name }, i) => {
                            return <Chip
                                icon={<Description style={{ color: 'blue' }} />}
                                variant="outlined" style={{ marginRight: 5 }}
                                label={name}
                                deleteIcon={<DeleteIcon />}
                                onDelete={() => {
                                    const filesClone = [...files]
                                    filesClone.splice(i, 1)
                                    setFiles(filesClone)
                                }}
                            />
                        })
                        : <Chip
                            icon={<Description style={{ color: 'gray' }} />}
                            label='0 archivos adjuntos'
                            variant="outlined" style={{ marginRight: 5 }}
                        />

                    }
                </div>
            }

            <div className={containerFile}>

                <>New File</>

                <TextField
                    label="File"
                    variant='standard'
                    type='file'
                    size='small'
                    className={input}
                    onChange={(e: any) => {
                        if (e.target.files[0]) setFileSelected(e.target.files[0])
                    }}
                />

                <div style={{ display: 'flex' }}>
                    <TextField
                        label="File name*"
                        variant='outlined'
                        size='small'
                        value={inputFile}
                        onChange={(e) => {
                            setInputFile(e.target.value)
                        }}
                        className={input}
                    />
                    <IconButton onClick={() => {
                        if (inputFile != '' && fileSelected) {
                            const filesClone = [...files]
                            filesClone.push({
                                name: inputFile,
                                file: fileSelected
                            })
                            setFiles(filesClone)
                            toast.success('Archivo subido correctamente, al agregar prospecto será almacenado')
                            setError('')
                            setInputFile('')
                            setFileSelected(null)
                        } else {
                            if (inputFile === '') setError('Favor de agregar un nobre al archivo')
                            if (!fileSelected) setError('Favor de seleccionar un archivo')
                        }

                    }}> <Save /> </IconButton>

                </div>
                <p className={errorMessage}>{error}</p>
            </div>

        </div>
    );
};
