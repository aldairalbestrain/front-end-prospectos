import * as React from 'react';
import { useHistory } from 'react-router';
import { ToastContainer } from 'react-toastify';
import { ContainerLogin } from '../components/Container';

interface ILoginPageProps {
}

export const LoginPage: React.FC<ILoginPageProps> = (props) => {

  const history = useHistory()

  const token = localStorage.getItem('token')
  if (token) history.push('/list')

  return (
    <div style={{ height: '100vh', background: '#fbf8f8' }}>
      <ContainerLogin />
      <ToastContainer />
    </div>
  );
};
