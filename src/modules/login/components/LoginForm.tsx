import { css } from '@emotion/css';
import { Button, TextField } from '@mui/material';
import * as React from 'react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { loginService } from '../../../services/loginService';
import { errorType } from './Container';

const inputClass = css`
    width: 100%;
    margin: 10px 0px !important;
`

const buttonClass = css`
    width: 100%;
    margin: 20px 0px !important;
`

interface LoadingFormType {
    setError: React.Dispatch<React.SetStateAction<errorType | null>>;
    loading: boolean;
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}

export const LoginForm: React.FC<LoadingFormType> = ({ setError, setLoading, loading }) => {
    const history = useHistory()
    const { register, handleSubmit, watch, formState: { errors } } = useForm();

    const onSubmit = async (data: any) => {
        setLoading(true)
        const dataResp = await loginService(data)

        if (!dataResp.error) history.push('/list')
        
        const status = dataResp?.error?.status || 0
        if( status === 400 || (status >= 405 && status < 500)) dataResp.error?.message.map(err=>{
            toast.error(err)
        })
        setError(dataResp.error)

        setLoading(false)
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <TextField
                label="Correo"
                className={inputClass}
                {...register('correo')}
                disabled={loading}
            />

            <TextField
                id="outlined-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                className={inputClass}
                {...register('password')}
                disabled={loading}
            />

            <Button variant="contained" type="submit" className={buttonClass} disabled={loading}>Entrar</Button>
        </form>
    );
};
