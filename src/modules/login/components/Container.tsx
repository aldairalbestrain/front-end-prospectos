import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { LoginForm } from './LoginForm';
import { LinearProgress, Typography } from '@mui/material';
import { css } from '@emotion/css';
import { ErrorBoundary } from '../../../shared/ErrorBoundary';

const container = css`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`

const cardClass = css`
    width: 375px;
    height: 300px;
    padding: 20px;
    position: relative;
`

export type errorType = {
    status: number,
    message: string[]
} | null

export const ContainerLogin: React.FC = (props) => {
    const [error, setError] = React.useState<errorType>(null)
    const [loading, setLoading] = React.useState(false)

    return (
        <ErrorBoundary error={error} setState={setError}>
            <div className={container}>
                <Card className={cardClass}>

                    {loading && <div style={{ position: 'absolute', top: 0, left: 0, width: '100%' }}><LinearProgress /></div>}

                    <Typography gutterBottom variant="h5" component="div" style={{ textAlign: 'center' }}>
                        Inicio de sesión
                    </Typography>
                    <CardContent>
                        <LoginForm setError={setError} setLoading={setLoading} loading={loading} />
                    </CardContent>
                </Card>
            </div>
        </ErrorBoundary>
    );
};
