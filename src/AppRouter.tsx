import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { LoginPage } from './modules/login/pages';
import { ProspectCapturePage } from './modules/prospectCapture/pages';
import { ProspectEvaluationPage } from './modules/prospectEvaluation/pages';
import { ProspectListPage } from './modules/prospectList/pages';

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/capture' component={ProspectCapturePage} />
        <Route exact path='/list' component={ProspectListPage} />
        <Route exact path='/evaluation/:id' component={ProspectEvaluationPage} />
        <Route exact path='/login' component={LoginPage} />
        <Route path='/' component={ProspectListPage} />
      </Switch>
    </BrowserRouter>
  )
}